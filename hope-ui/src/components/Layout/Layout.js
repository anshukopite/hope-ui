import React, { Component } from 'react'
import Auxillary from '../../hoc/Auxillary';
import Header from '../fixed-components/Header';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from '../Login/Login';
import Footer from '../fixed-components/Footer';

class Layout extends Component {

    render() {
        return (
            <Auxillary>
                <div>Toolbar,Sidebar,BackDrop</div>
                <Router>
                    <>
                        <Header />
                        <Switch>
                            <Route path="/" exact component={Login} />
                            <Route path="/login" component={Login} />
                        </Switch>
                        <main>
                            {this.props.children}
                        </main>
                        <Footer/>
                    </>
                </Router>
            </Auxillary>
        );
    }
}

export default Layout;