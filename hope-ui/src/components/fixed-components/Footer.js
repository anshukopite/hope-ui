import React, { Component } from 'react'

class Footer extends Component {
    render() {
        return (
            <footer className="footer">
                <span className="text-muted">All Rights Reserved 2018 @hope.org</span>
                <span >About Us</span>
            </footer>
        )
    }
}

export default Footer