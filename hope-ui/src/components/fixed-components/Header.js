import React, { Component } from 'react'
import {Link} from 'react-router-dom'
class Header extends Component {
    render() {
        const isUserLoggedIn = true;

        return (
            <header>
                <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                    <div><a href="http://www.hope.org" className="navbar-brand">HOPE</a></div>
                    <ul className="navbar-nav">
                        {isUserLoggedIn && <li><Link className="nav-link" to="/welcome/in28minutes">Dashboard</Link></li>}
                        {isUserLoggedIn && <li><Link className="nav-link" to="/todos">Profile</Link></li>}
                        {isUserLoggedIn && <li><Link className="nav-link" to="/todos">Raise Request</Link></li>}
                    </ul>
                    <ul className="navbar-nav navbar-collapse justify-content-end">
                        {!isUserLoggedIn && <li><Link className="nav-link" to="/login">Login</Link></li>}
                        {isUserLoggedIn && <li><Link className="nav-link" to="/logout" onClick="#">Logout</Link></li>}
                    </ul>
                </nav>
            </header>
        )
    }
}

export default Header